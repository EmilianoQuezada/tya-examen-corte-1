async function cargarCocteles() {
    try {
        const tempUrl = 'https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=';
        const tipos = document.getElementsByName('radio-group');
        let seleccion;

        for (let i = 0; i < tipos.length; i++) {
            if (tipos[i].checked) {
                seleccion = tipos[i].value;
                break;
            }
        }

        if (!seleccion) {
            console.error("Debes seleccionar un tipo de coctel.");
            return;
        }

        const response = await fetch(tempUrl + seleccion);
        const data = await response.json();

        const galeria = document.getElementById('galeria');

        galeria.innerHTML = "";

        let cantidadCocteles = data.drinks.length;

        data.drinks.forEach(coctel => {
            if (coctel.strDrinkThumb && coctel.strDrink) {
                const cajaImagen = document.createElement('div');
                cajaImagen.classList.add('caja-imagen');

                const imagen = document.createElement('img');
                imagen.src = coctel.strDrinkThumb;
                imagen.alt = coctel.strDrink;

                const descripcion = document.createElement('small');
                descripcion.textContent = coctel.strDrink;

                cajaImagen.appendChild(imagen);
                cajaImagen.appendChild(descripcion);
                galeria.appendChild(cajaImagen);
            }
        });

        const labelCantidadCocteles = document.getElementById('coctelesCantidad');
        labelCantidadCocteles.textContent = `Se muestran ${cantidadCocteles} cocteles.`;
    } catch (error) {
        console.error("Hubo un error al cargar los cocteles:", error);
    }
}

document.getElementById('btnCargar').addEventListener('click', cargarCocteles);




document.getElementById('btnCargar').addEventListener('click',function(){
    cargarCocteles()
})